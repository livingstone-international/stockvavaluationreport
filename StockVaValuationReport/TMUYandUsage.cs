﻿using Livingstone.DBLib;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockVaValuationReport
{
    public class TMUYandUsage
    {
        private static IDBHandler dBHandler = new SqlServerDB();

        public static void processTMUYandUsageTables(DataTable itemdt)
        {

            #region clean tables

            string DropandCreateSql = @"
                             
                            if(object_id('[backup].[dbo].TMUQYtemp')) is not null
                            drop table [backup].[dbo].TMUQYtemp

                            CREATE TABLE [Backup].[dbo].[TMUQYtemp](
	                            [item_no] [nvarchar](50) NULL,
	                            [TMUQY] [nvarchar](50) NULL,
	                            [StockMonth] [nvarchar](50) NULL,
	                            [StockMonthTriple] [nchar](50) NULL
                            ) ON [PRIMARY]

                            if(object_id('[backup].[dbo].TotalGrowthTemp')) is not null
                            drop table [backup].[dbo].TotalGrowthTemp

                            CREATE TABLE [Backup].[dbo].[TotalGrowthTemp](
	                            [item_no] [nvarchar](50) NULL,
	                            [TotalGrowth] [nvarchar](50) NULL,
	                            [TotalGrowthNum] decimal(18,4) default 0
                            ) ON [PRIMARY] ";


            dBHandler.ExecuteNonQuery(DropandCreateSql, SqlServerDB.Servers.netcrmau);

            #endregion


            if (itemdt.Rows.Count > 0)
            {
                for (int i = 0; i < itemdt.Rows.Count; i++)
                {

                    Dictionary<string, object> itemNoParam = new Dictionary<string, object>();
                    itemNoParam.Add("@itemNo", itemdt.Rows[i][0].ToString().Trim());


                    LoadTotalGrowthAndTMUQYandUsage(itemNoParam);

                }

            }


            SyncToNetCRM();
        }

        private static void SyncToNetCRM()
        {
            string sql  = @"
                        truncate table [TMUQYAndStockMonth]

                        insert into[TMUQYAndStockMonth]([item_no],[TMUQY],[StockMonth],[StockMonthTriple])
                        Select[item_no],[TMUQY],[StockMonth]
                        ,convert(decimal(18, 2), ltrim(rtrim(replace(stockMonthtriple, ',', '')))) as [StockMonthTriple]
                        from[backup].[dbo].TMUQYtemp";

            dBHandler.ExecuteNonQuery(sql, SqlServerDB.Servers.netcrmau);
        }



        private static void LoadTotalGrowthAndTMUQYandUsage(Dictionary<string, object> itemNoParam)
        {


            YTDStatistics ytdTsk = loadYTD(itemNoParam);
            int ptdTsk = loadPTD(itemNoParam);

            YTDStatistics stocks = ytdTsk;
            //var stockTsk = loadSafetyStock(itemNoParam, stocks.stockMonth, stocks.stockMonthTriple);//not necessary for growth

            var totalMonthlyUsgLast = ptdTsk;
            var totalCurrentMonthUsage = stocks.totalCurrentMonthUsage;
            var dblTotalGrowth = (decimal)(totalCurrentMonthUsage - totalMonthlyUsgLast) / (totalMonthlyUsgLast == 0 ? 1 : totalMonthlyUsgLast) * 100;


            string txtTotalGrowth = dblTotalGrowth.ToString("N2") + " %";

            decimal dblTotalGrowthNum = (decimal)(totalCurrentMonthUsage - totalMonthlyUsgLast) / (totalMonthlyUsgLast == 0 ? 1 : totalMonthlyUsgLast) ;

            String insertSql = @"insert into [backup].dbo.TotalGrowthTemp(item_no,TotalGrowth, TotalGrowthNum)
                                values('" + itemNoParam["@itemNo"].ToString().Trim() + "', '" + txtTotalGrowth.ToString().Trim() + "', " + dblTotalGrowthNum + ") ";


            dBHandler.ExecuteNonQuery(insertSql, SqlServerDB.Servers.netcrmau);
        }
        private struct YTDStatistics
        {
            public int totalCurrentMonthUsage;
            public decimal stockMonth;
            public decimal stockMonthTriple;

            public YTDStatistics(int tmut, decimal sm, decimal smq)
            {
                totalCurrentMonthUsage = tmut;
                stockMonth = sm;
                stockMonthTriple = smq;
            }
        }


        private static YTDStatistics loadYTD(Dictionary<string, object> itemNoParam)
        {
            #region query
            string subQuery =
            @"select 
	            a.loc as Warehouse, 
	            CAST(qty_sold_ytd AS DECIMAL(18,0)) as [Quantity Sold], 
	            CAST(usage_ytd AS DECIMAL(18,0)) as [Usage], 
	            sls_ytd as [Sales], 
	            cost_ytd as [Cost], 
                CASE WHEN a.loc = 'P'  THEN 1 
		                WHEN a.loc = 'T'  THEN 2 
		                WHEN a.loc = 'V'  THEN 3 
		                WHEN a.loc = 'PM' THEN 4 
                        WHEN a.loc = 'CM' THEN 5
                        WHEN a.loc = 'CV' THEN 6 
		                WHEN a.loc = 'TT' THEN 7
		                    ELSE 8 
                    end
		            AS [Order], 
	            CAST( round( CEILING(usage_ytd/@MonCount), 0 )  --by ted 2012/11/02
                    as int) AS [Monthly Usg], 
	            ISNULL(
                    (   select top 1 netusage 
                        from  po_NetUsageTable_withRepack
                        where po_NetUsageTable_withRepack.item_no = a.item_no 
			                and po_NetUsageTable_withRepack.loc = a.loc 
		            ), 0) as [NetUageReorderRepack],
                usage_ytd/@MonCount AS [MonthlyUsg], 
                ISNULL(CAST((Tag_cost) as DECIMAL(18,1)), 0) AS [ReorderRepack3mth]
            , a.item_no , a.loc
            from iminvloc_sql a 
            where item_no = @itemNo ";

            string ytdQuery = @"
            set transaction isolation level READ UNCOMMITTED
            DECLARE @MonCount decimal(18,2);
            SET @MonCount = (MONTH(GETDATE())+5)%12 + DAY(GETDATE())/30.0;
            IF @MonCount < 1 SET @MonCount = 1;

            select 
                b.Warehouse
                , b. [Quantity Sold]
                , b.[Usage]
                , b.[Sales]
                , b.[Cost]
                , b.[Order]
                , b.[Monthly Usg]
                , b.[NetUageReorderRepack] as [TMUYwithRepack]
                , b.[MonthlyUsg]
                , b.[ReorderRepack3mth]
                ,  --case when priority_group in ('P1','P2') then  ReorderRepack3mth - c.usg_qty  else  
                    NetUageReorderRepack - c.usg_qty   -- end  
                    as [TMU Ex SQ]
                , cast(  (  b.[NetUageReorderRepack] + b.[ReorderRepack3mth] ) / 2.0   as decimal(18,1)) as [TMUQY (Q+Y)/2]
                , suggest_order_exl_sql + isnull(sq, 0) [suggest_order_v3]
            from ( 
                    " + subQuery + @"
                ) b left outer join sq_reorder_v3 c on (c.item_no =  b.item_no and   c.loc = b.loc)
                left outer join imitmidx_sql d on (b.item_no = d.item_no)
                left outer join vReorder_v2 e on (b.item_no = e.item_no and  e.loc = b.loc)
            ORDER BY [ORDER];";

            const string whLstQuery = @"exec GetStockMonthTMUQYWarehouse";

            const string qohQuery = @"SELECT SUM(qty_on_hand) FROM IMINVLOC_SQL WHERE item_no = @itemNo AND loc IN (select loc_code from warehouse where Status = 'Active' and stock = 1 );";
            #endregion
            DataTable YTD = new DataTable();
            List<List<string>> whList = new List<List<string>>();

            dBHandler.getDataList(YTD, ytdQuery, SqlServerDB.Servers.netcrmau, itemNoParam);
            dBHandler.getDataList(null, whList, null, whLstQuery, SqlServerDB.Servers.netcrmau);
            decimal? qohtask = dBHandler.getDecimal(qohQuery, SqlServerDB.Servers.netcrmau, itemNoParam);

            //Perform statistics
            int totalCurrentMonthUsage = 0;
            int ytdMonthlyUsg = 0;
            decimal totalLastThreeMonthReorderRepack = 0;
            decimal totalMonthlyUsageTriple = 0;
            decimal quarterMonthlyUsg = 0;
            decimal tmuyWithRepack = 0;


            HashSet<string> whHash = new HashSet<string>() { "TEST" };
            foreach (var row in whList)
                if (row.Count != 0)
                    whHash.Add(row[0]);

            //sum for usage and reorders based on warehouse locations
            for (int row = 0; row < YTD.Rows.Count; row++)
            {
                var wh = (YTD.Rows[row]["Warehouse"] as string).Trim().ToUpper();
                if (whHash.Contains(wh))
                {
                    ytdMonthlyUsg += TypeConversion.toInt32(YTD.Rows[row]["MonthlyUsg"]);
                    quarterMonthlyUsg += TypeConversion.toDecimal(YTD.Rows[row]["ReorderRepack3mth"]);
                    tmuyWithRepack += TypeConversion.toDecimal(YTD.Rows[row]["TMUYwithRepack"]);
                }
                if (!wh.StartsWith("Y") && !wh.StartsWith("XHK"))
                {
                    var reorder = TypeConversion.toDecimal(YTD.Rows[row]["ReorderRepack3mth"]);
                    totalLastThreeMonthReorderRepack += reorder * 3;
                    totalMonthlyUsageTriple += reorder;
                    totalCurrentMonthUsage += TypeConversion.toInt32(YTD.Rows[row]["Monthly Usg"]);
                }
            }

            decimal qoh = qohtask ?? 0;
            decimal stockMonthTriple = quarterMonthlyUsg <= 0 ? 0 : Math.Floor(qoh / quarterMonthlyUsg * 10) / 10; // round down to 1 decimal place. e.g. 4.99 -> 4.9, 5.01 -> 5.0
            decimal usage = (tmuyWithRepack + totalMonthlyUsageTriple) / 2;//amy ask to divide by 2 to make it more accurate
            decimal stockMonth = usage <= 0 ? 0 : Math.Floor(qoh / usage * 10) / 10;

            string txtStockMonthTriple = stockMonthTriple.ToString("N1"); //stock month in middle       
            string txtMonthlyUsageTriple = totalMonthlyUsageTriple.ToString("N0"); //TMUQ


            string txtTMUQY = usage.ToString();
            string txtStockMonth = stockMonth.ToString("N1");

            String insertSql = @"insert into [backup].dbo.TMUQYtemp(item_no,TMUQY,StockMonth,StockMonthTriple )
                                values('" + itemNoParam["@itemNo"].ToString().Trim() + "', '" + txtTMUQY.ToString().Trim() + "', '" + txtStockMonth.ToString().Trim() + "', '" + txtStockMonthTriple.ToString().Trim() + "') ";


            dBHandler.ExecuteNonQuery(insertSql, SqlServerDB.Servers.netcrmau);


            return new YTDStatistics(totalCurrentMonthUsage, stockMonth, stockMonthTriple);
        }



        private static int loadPTD(Dictionary<string, object> itemNoParam)
        {
            #region query
            string sqlPTD =
                @"SELECT  
                    loc AS Warehouse ,
                    CAST(qty_sold_last_yr AS DECIMAL(18, 0)) AS [Quantity Sold] ,
                    CAST(prior_year_usage AS DECIMAL(18, 0)) AS [Usage] ,
                    prior_year_sls AS [Sales] ,
                    cost_last_yr AS [Cost] ,
                    CASE WHEN loc = 'P'  THEN 1 
		                 WHEN loc = 'T'  THEN 2 
		                 WHEN loc = 'V'  THEN 3 
		                 WHEN loc = 'PM' THEN 4 
                         WHEN loc = 'CM' THEN 5
                         WHEN loc = 'CV' THEN 6 
                           WHEN loc = 'TT' THEN 7
		                 ELSE 8 
                    END AS [Order] ,
                    CAST(ROUND(( CAST(prior_year_usage AS DECIMAL(18, 0)) / 12 ), 0) AS INT) AS [Monthly Usg]
                FROM    iminvloc_sql
                WHERE   item_no = @itemNo";


            sqlPTD += " ORDER BY [ORDER]";
            #endregion

            DataTable PTD = new DataTable();
            dBHandler.getDataList(PTD, sqlPTD, SqlServerDB.Servers.netcrmau, itemNoParam);
            int intTotalMonthlyUsgLast = 0;
            for (int row = 0; row < PTD.Rows.Count; row++)
                intTotalMonthlyUsgLast += TypeConversion.toInt32(PTD.Rows[row]["Monthly Usg"]);

            return intTotalMonthlyUsgLast;
        }


        private async Task loadSafetyStock(Dictionary<string, object> itemNoParam, decimal stockMonth, decimal stockMonthTriple)
        {
            #region query
            const string safetyQuery = @"
            select SUM(a.net_usage) net_usage, 
	            MIN(a.class) class, 
	            MIN(a.safety_stock_month) safety_stock_month,
	            max(b.var_lead_tm ) stockholding,
	            max(b.planning_lead_tm)  leadtime, 
	            max(b.var_lead_tm ) + max(b.planning_lead_tm)  as  safety_stock_level
            from summaryofreorderreport a 
	            left outer join imitmidx_sql b on (a.item_no = b.item_no )      
            where a.item_no = @itemNo; ";

            const string dutyQuery = @"
            select top 1          
	            Rate,           
	            Tariff          
            From tblDutyRates           
            Where ItemNo = @ItemNo     
            order by tariffyear desc, tariffmonth desc;";

            const string classQuery = @"SELECT MIN(class) class, MIN(safety_stock_month) safety_stock_month FROM SummaryOfInventory WHERE item_no = @itemNo;";
            #endregion

            List<object> safetyData = new List<object>();
            List<string> dutyData = new List<string>();
            List<string> classData = new List<string>();
            var safetyTsk = dBHandler.getDataListAsync(null, safetyData, null, safetyQuery, SqlServerDB.Servers.netcrmau, itemNoParam);
            var dutyTsk = dBHandler.getDataListAsync(null, dutyData, null, dutyQuery, SqlServerDB.Servers.netcrmau, itemNoParam);
            var classTsk = dBHandler.getDataListAsync(null, classData, null, classQuery, SqlServerDB.Servers.netcrmau, itemNoParam);
            await Task.WhenAll(safetyTsk, classTsk).ConfigureAwait(false);

            var reorderLevel = TypeConversion.toDecimalNull(safetyData[5]) ?? 0;

        }





  



    }
}
