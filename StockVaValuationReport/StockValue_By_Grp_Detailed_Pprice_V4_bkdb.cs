﻿using Livingstone.DBLib;
using StockVaValuationReport.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockVaValuationReport
{
    public class StockValue_By_Grp_Detailed_Pprice_V4_bkdb
    {
        private static IDBHandler dBHandler = new SqlServerDB();
        private static string MonthFirstDay = DateTime.Now.Year + DateTime.Now.Month.ToString("00") + "01";
        private static string backupStockValueTb="[backup].dbo.stock_value_v4_BKdb_" + MonthFirstDay;



        private static string OldStockValueReportsql = @"

                    IF object_id('[backup].[dbo].tempMonthReport') IS not  NULL 
                    drop table [backup].[dbo].tempMonthReport

                    SELECT
                        stock_value_v4_BKdb.item_no, stock_value_v4_BKdb.cost1, 
	                    stock_value_v4_BKdb.cost2, stock_value_v4_BKdb.cost8, 
	                    stock_value_v4_BKdb.cost30, stock_value_v4_BKdb.cost3, 
	                    stock_value_v4_BKdb.cost4, stock_value_v4_BKdb.cost5, 
	                    stock_value_v4_BKdb.cost6, stock_value_v4_BKdb.cost7, 
	                    stock_value_v4_BKdb.avg_wghted_cost2,

                        IMINVLOC_SQL.last_cnt_dt, IMINVLOC_SQL.oldest_cnt_dt, IMINVLOC_SQL.noOfBin, IMINVLOC_SQL.item_filler,
	                    IMINVLOC_SQL.loc, IMINVLOC_SQL.qty_on_hand, IMINVLOC_SQL.qty_allocated, IMINVLOC_SQL.qty_bkord, 
	                    IMINVLOC_SQL.qty_on_ord, IMINVLOC_SQL.qty_sold_ytd,
                        IMITMIDX_SQL.GRA_aging, IMITMIDX_SQL.SO_aging, IMITMIDX_SQL.item_desc_1, IMITMIDX_SQL.item_desc_2,
	                     IMITMIDX_SQL.last_item_revision, IMITMIDX_SQL.consumed_fg, IMITMIDX_SQL.Priority_Group, 
	                     IMITMIDX_SQL.highest_sold_price, IMITMIDX_SQL.lowest_sold_price, IMITMIDX_SQL.ageing_rank,
                        IMCATFIL_SQL.prod_cat, IMCATFIL_SQL.prod_cat_desc,
                        items.CostPriceStandard,
                        SummaryOfInventory.class
                    into [backup].[dbo].tempMonthReport
                    FROM
                        { oj (((("+backupStockValueTb+@" stock_value_v4_BKdb INNER JOIN netcrm.dbo.vImitmidx_sql IMITMIDX_SQL ON
                            stock_value_v4_BKdb.item_no = IMITMIDX_SQL.item_no)
                         INNER JOIN netcrm.dbo.vIMINVLOC_SQL_stockreport IMINVLOC_SQL ON
                            stock_value_v4_BKdb.item_no = IMINVLOC_SQL.item_no)
                         INNER JOIN netcrm.dbo.IMCATFIL_SQL IMCATFIL_SQL ON
                            IMINVLOC_SQL.item_filler = IMCATFIL_SQL.prod_cat)
                         LEFT OUTER JOIN netcrm.dbo.SummaryOfInventory SummaryOfInventory ON
                            IMINVLOC_SQL.item_no = SummaryOfInventory.item_no AND
                        IMINVLOC_SQL.loc = SummaryOfInventory.loc)
                         LEFT OUTER JOIN netcrm.dbo.items items ON
                            IMITMIDX_SQL.item_no = items.ItemCode}
                    WHERE
                        (IMCATFIL_SQL.prod_cat <> '66V' AND
                        IMCATFIL_SQL.prod_cat <> '72' AND
                        IMCATFIL_SQL.prod_cat <> '72T' AND
                        IMCATFIL_SQL.prod_cat <> '999')
                    ORDER BY
                        IMINVLOC_SQL.loc ASC,
                        IMINVLOC_SQL.item_filler ASC,
                        stock_value_v4_BKdb.item_no ASC

                    IF object_id('[backup].[dbo].MonthReport') IS not  NULL 
                    drop table [backup].[dbo].MonthReport

                    select a.loc,a.prod_cat, a.prod_cat_desc,
                    a.item_no, 
                    isnull(a.item_desc_1,'') + '' + isnull(a.item_desc_2,'') as GroupDescription, 
                    a.qty_on_hand, a.qty_allocated as [O/S], 
                    a.qty_sold_ytd AS [O/Y], 
                    a.qty_bkord as [B/O],
                    a.qty_on_ord as [P/O], 
                    a.cost8 as pprice,
                    a.costpricestandard as std, 
                    a.cost8* a.qty_on_hand as ppriceAmount,
                    a.qty_on_hand*CostPriceStandard as stdAmount,

                    a.last_item_revision,
                    a.class, a.Priority_group, a.SO_aging, 
                    a.GRA_aging,a.ageing_rank,
                    a.highest_sold_price, a.lowest_sold_price,
                    a.last_cnt_dt, a.oldest_cnt_dt, a.avg_wghted_cost2
                    ,a.noOfBin
                    into [backup].[dbo].MonthReport
                    from [backup].[dbo].tempMonthReport a
                    where a.loc in ('BA','BC','CM','CV','P','PC','PM','T','TT','V','VK','VB')
                    order by a.loc, a.prod_cat asc
           ";
        // get unique
        // item_no,  need to add some more items with stock, these are used in other project
        private static string uniqueItemsSql = @"
                                                    if object_id('tempdb..#itemstocktable') is not null 
                                                     drop table #itemstocktable;

                                                                WITH cte_stock AS
                                                                (
	                                                                select v.item_no,
		                                                                v.[On Hand],
		                                                                v.WareHouse as loc,
		                                                                v.BO,
		                                                                v.[On PO],
		                                                                v.Allocated,
		                                                                v.[12 Months Sls],
		                                                                v.[12 Mos Usage],
		                                                                v.[Status],
                                                                        v.[Order]
	                                                                from vitem_stock_details1 v
	                                                               
                                                                )
                                                     SELECT  cte_stock.item_no, rtrim(cte_stock.loc) as WH,
                                                             CASE WHEN bom.[type] = 'KIT' THEN kitqty.qty
		                                                      ELSE cte_stock.[On Hand] END  AS [Onhand]

                                                    into #itemstocktable
                                                    FROM cte_stock
                                                    LEFT JOIN vKITBOMItem AS bom
                                                    ON bom.item_no = cte_stock.item_no
                                                    LEFT JOIN vkititemqty1 AS kitqty
                                                    ON kitqty.item_no = cte_stock.item_no AND kitqty.loc = cte_stock.loc

                                                    Select item_no, sum(Onhand) stock
                                                    into #itemstocktableSum
                                                    from #itemstocktable a 
                                                    where Onhand > 0 
                                                          and wh in (select loc_code from warehouse where [status] = 'active' and stock =1 )
                                                    group by item_no

	                                                    Select distinct item_no
	                                                    from #itemstocktableSum
	                                                    union 
	                                                    select distinct item_no 
	                                                    from [backup].[dbo].MonthReport    ";

        private static string combinedTableSql = @"                                                                                                                                                      
                                                        select 
                                                               a.[prod_cat]   
                                                              ,a.[prod_cat_desc]
	                                                          ,a.[loc]  
                                                              ,a.[item_no]
	                                                          ,isnull(f.[status],'')  as [status]
	                                                          ,isnull(g.ItemType,'')  as ItemType
                                                              ,a.[GroupDescription]
                                                              ,a.[qty_on_hand]
                                                              ,a.[O/S]
                                                              ,a.[O/Y]
                                                              ,a.[B/O]
                                                              ,a.[P/O]
                                                              ,a.[pprice]
                                                              ,a.[std]
                                                              ,a.[ppriceAmount]
                                                              ,a.[stdAmount]
                                                              ,a.[last_item_revision]
                                                              ,a.[class]
                                                              ,a.[Priority_group]
                                                              ,a.[SO_aging]
                                                              ,a.[GRA_aging]
                                                              ,a.[ageing_rank]
                                                              ,a.[highest_sold_price]
                                                              ,a.[lowest_sold_price]
                                                              ,a.[last_cnt_dt]
                                                              ,a.[oldest_cnt_dt]
                                                              ,a.[avg_wghted_cost2]
                                                              ,a.[noOfBin]
	  
	                                                          , b.TMUQY,b.StockMonth 
	                                                          , c.[On Hand] 

	                                                          , isnull(d.totalQtySold,0)   totalQtySold 
	                                                          , isnull(d.totalSales,0) totalSales
	                                                          ,isnull(d.last_sold_dt,'') last_sold_dt
	                                                          ,e.TotalGrowth
	                                                          ,h.*

                                                        from [backup].[dbo].MonthReport a
                                                        left join [backup].[dbo].TMUQYtemp b
                                                        on(a.item_no = b.item_no)
                                                        left join [backup].[dbo].TotalqtyTableTemp c
                                                        on(a.item_no = c.item_no)
                                                        left join [backup].[dbo].itemsalesTemp d
                                                        on(a.item_no =d.productCode)
                                                        left join [backup].[dbo].TotalGrowthTemp e
                                                        on(a.item_no = e.item_no)
                                                        left join
                                                        (

	                                                        SELECT DISTINCT tb1.item_no, tb1.loc,
			                                                           CASE WHEN tb1.status NOT IN ('A', 'K') THEN tb1.[status]
				                                                            WHEN tb1.status <> 'K' AND  tb2.visible_from IS NOT NULL AND tb2.visible_from > GETDATE() THEN 'X'
					                                                        ELSE tb1.[status] END [status]
		                                                        FROM iminvloc_sql AS tb1
		                                                        LEFT JOIN tblindexinfo_itemno AS tb2
		                                                        ON tb1.item_no = tb2.item_no

                                                        ) f
                                                        on(a.item_no = f.item_no and a.loc = f.loc)
                                                        left join
                                                        (
                                                               select distinct
                                                                a.item_no,

                                                                [ItemType]=case
                                                                    when b.kit_feat_fg = 'K' then
                                                                        'KIT'
                                                                    else
                                                                        'BOM'
                                                                end,
                                                                a.comp_item_no,
       
                                                                a.qty_per_par
		                                                        --into #testbom
                                                            from bmprdstr_sql a
                                                            join imitmidx_sql b
                                                                on a.item_no = b.item_no 
                                                        ) g
                                                        on(a.item_no = g.item_no)
                                                        left join stock_value_cv_view h
                                                        on(a.item_no = h.cvItems)

                                                        where   a.[prod_cat]  NOT  in ('FIA','07D','XXX','S01', '65S' )
                                                               and a.item_no not in (select item_no from [stock_value_remove_items])
	                                                           and a.[qty_on_hand] >=0
	                                                           and isnull(f.[status],'')  not in ('S','C','K')--remove sample, cancel and kit
	                                                           AND isnull(g.ItemType,'') NOT IN ('BOM')
	                                                           and GroupDescription not like ('*%')
	                                                           and GroupDescription not like ('out of date%')
                                                        order by a.prod_cat asc, a.loc asc ";


        public static void RunReport()
        {
            
            //1 run old stock report and get table MonthReport
            
            dBHandler.ExecuteNonQuery(OldStockValueReportsql, SqlServerDB.Servers.netcrmau);


            #region calculate TMUY and Usage for NetCRM, so must prepare item list first(items with stock)

            //2 get all unique item list
            DataTable uniqueItems = new DataTable();
            dBHandler.getDataList(uniqueItems, uniqueItemsSql, SqlServerDB.Servers.netcrmau);

            //3 pass item_no table to TMUQY program to get another two tables, TMUQYtemp and TotalGrowthTemp
            TMUYandUsage.processTMUYandUsageTables(uniqueItems);

            #endregion

            //4 create tables TotalqtyTableTemp and itemsalesTemp  
            QuantityAndSales.CreateQuantityAndSalesTables(); 

            //5 join all tables and output to a excel file
            DataTable combinedTable = new DataTable();
            dBHandler.getDataList(combinedTable, combinedTableSql, SqlServerDB.Servers.netcrmau);

            string MachineName = String.IsNullOrEmpty(System.Environment.MachineName) ? "" : System.Environment.MachineName;
            string fileName = DateTime.Now.Year.ToString() + DateTime.Now.ToString("MM") + DateTime.Now.ToString("dd") +
                "_" + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + MachineName +".xlsx";


            string fileLocation = @"\\livdoc9\Mydocs\IT\Adrian\MarcusDailyReport\";
            ExportToExcel.DataToExcel(combinedTable, "combinedRep", fileLocation + fileName,  true,null);

            //fileName = "20210317_1214UATT32.xlsx";
            CheckResultsAndSendEmail(fileLocation, fileName);
        }




        public static void CheckResultsAndSendEmail(string fileLocation, string fileName)
        {
            if (File.Exists(fileLocation + fileName))
            {
                string emailMessage = fileLocation + "       File generated: " + fileName + Environment.NewLine;
                Tools.SendEmail("Report Finished", emailMessage);
            }
            else
            {
                Tools.SendEmail("Report Failed","");
            }

        }








    }




}
