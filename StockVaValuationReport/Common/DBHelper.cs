﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockVaValuationReport.Common
{
    public class DBHelper
    {
        public static SqlConnection getConn(string server)
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConfigurationManager.AppSettings[server];
            con.Open();

            return con;
        }

        public static DataTable GetTableFrom(string sql, SqlConnection con)
        {
            DataTable table = new DataTable();
            //SqlConnection con = new SqlConnection();
            //con.ConnectionString = ConfigurationManager.AppSettings[source];
            SqlCommand com = con.CreateCommand();
            com.CommandType = CommandType.Text;
            com.CommandText = sql;
            com.CommandTimeout = 120000;
            SqlDataAdapter sqldata = new SqlDataAdapter();
            try
            {
                //con.Open();
                sqldata.SelectCommand = com;
                sqldata.Fill(table);
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
            }
            finally
            {
                //if (con.State == ConnectionState.Open) con.Close();
                com.Dispose();
                //con.Dispose();
            }
            return table;
        }
    }
}
