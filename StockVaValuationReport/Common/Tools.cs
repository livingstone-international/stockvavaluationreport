﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace StockVaValuationReport.Common
{
    public class Tools
    {
        public static void SendEmail(String subject, string body)
        {
            string subjectStr = "Notice: " + new String(subject.Take(50).ToArray());
            string bodyStr = body.Length > 1 ? body : subject;
            try
            {
                string sendFrom = ConfigurationManager.AppSettings["email1"];
                string passencrypted =  getMailPasswordFromDB(sendFrom);         
                string sendFromPass =DataProtectService.Decrypt(passencrypted, "l0cksm1th");


                MailMessage message = new MailMessage();
                SmtpClient smtp = new SmtpClient();
                //message.From = new MailAddress("email@livingstone.com.au");
                message.From = new MailAddress(sendFrom);
                message.To.Add(new MailAddress("reportmonitor@livingstone.com.au"));
                message.Subject = subjectStr;
                message.Body = bodyStr;
                message.IsBodyHtml = true;
                smtp.Port = 587;
                smtp.Host = "smtp.office365.com";
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                //smtp.Credentials = new NetworkCredential("email@livingstone.com.au", "Arthur123");
                smtp.Credentials = new NetworkCredential(sendFrom, sendFromPass);
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

                smtp.Send(message);
            }
            catch (Exception e)
            {

                SendEmailAdmin(subjectStr, bodyStr);
            }
        }
        private static string getMailPasswordFromDB(string email)
        {
            string sql = @"spGetEmailPasswordInSenderAccount '"+email+"'";

            object o;
            using(SqlConnection conn = DBHelper.getConn("NetCRMAU"))
            {
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                      o = cmd.ExecuteScalar();
                }

            }
            return o.ToString();

        }


        public static void SendEmailAdmin(String subject, string body)
        {

            string dbmail = @"EXEC SendEmailWithRecipientSubjectBody
                                    @subject = '" + subject + @"',
                                    @body = '" + body + @"' ,
                                    @email = 'reportmonitor@livingstone.com.au'
                               ";//
            try
            {          
                using (SqlConnection conn = DBHelper.getConn("NetCRMAU"))
                {
                    using (SqlCommand cmd = new SqlCommand(dbmail, conn))
                    {
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {

            }

        }


    }
}
