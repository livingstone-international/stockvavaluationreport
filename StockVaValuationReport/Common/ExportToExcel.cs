﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeOpenXml;

namespace StockVaValuationReport.Common
{
    public static class ExportToExcel
    {
        public static void DataToExcel(DataTable dataTable, string title, string tempPath,
           bool printHeaders = true,IList<int?> widths = null)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                if (dataTable == null)
                {
                    return;
                }

                using (ExcelPackage xlWBK = new ExcelPackage())
                {
                    if (title == null)
                    {
                        title = "Sheet1";
                    }
                    var xlWSt = xlWBK.Workbook.Worksheets.Add(title);
                    xlWSt.Cells["A1"].LoadFromDataTable(dataTable, printHeaders);

                    for (int col = 1; col < dataTable.Columns.Count; col++)
                    {
                        if (dataTable.Columns[col - 1].DataType == typeof(DateTime))
                        {
                            xlWSt.Column(col).Style.Numberformat.Format = "dd/MM/yyyy hh:mm:ss AM/PM";
                        }
                        if(widths != null && col < widths.Count && widths[col] != null && widths[col] >0)
                        {
                            for(int row =2; row<dataTable.Columns.Count +2;row++)
                            {
                                xlWSt.Column(col).Width = widths[col].Value;
                            }
                        }


                    }
                    xlWBK.SaveAs(ms);

                }

                FileInfo tempFileInfo = new FileInfo(tempPath);
                using (FileStream fs = tempFileInfo.OpenWrite())
                {
                    ms.Position = 0;
                    ms.CopyTo(fs);
                }

            }
        }



    }
}
