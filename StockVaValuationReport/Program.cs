﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Livingstone.DBLib;
namespace StockVaValuationReport
{
    class Program
    {
        //note: you need to make the program can be run many times, need to check whether this section already run.
        // add section to call crystal report
        /*
         * 
         run report on xa19         /Reports/Products/Stock Value_details_v4       
         (missing the first day --> Stock Value_details_v4 ME, must execute month end files )
            8 years/STOCK WH     excel version 7 and  with heading  
           no matter which metafram                    
        */

        private static IDBHandler dBHandler = new SqlServerDB();
        static void Main(string[] args)
        {
            MonthStartProcess.CheckAndRunProcess();

            StockValue_By_Grp_Detailed_Pprice_V4_bkdb.RunReport();
          
        }

     

    }
}
