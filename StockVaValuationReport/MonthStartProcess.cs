﻿using Livingstone.DBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockVaValuationReport
{
    public static class MonthStartProcess
    {
        private static IDBHandler dBHandler = new SqlServerDB();

        public static void CheckAndRunProcess()
        {
            if(DateTime.Now.Day == 1 )
            {
                RunProcess();
            }

        }


        private static void RunProcess()
        {
            //create date for end of last month and last last month, test now, should be one 
            var now = DateTime.Now;
            var lastDayLastMonth = new DateTime(now.Year, now.Month, 1).AddDays(-1);
            var lastDayLastLastMonth = new DateTime(now.Year, now.Month, 1).AddMonths(-1).AddDays(-1);

            //create backup for you tables. 
            BackupTable bk = new BackupTable(lastDayLastMonth, lastDayLastLastMonth, "start");
            //bk.TranHistorybackupTB;
            // for test only
            //bk.TranHistorybackupTB = "[backup].dbo.Liv_vu_ItemTransactionHistory_20190731";         

            // 1  create new         dbo.item_unit_cost_per_po_per_receive_dt_avg2  can be run multiple time 

            Dictionary<String, String> param1 = new Dictionary<String, string>()
            {
                {"@TranHistorybackupTB", bk.TranHistorybackupTB }
            };
            String create_item_unit_cost_per_po_per_receive_dt_avg2_sql =
                   @"

                        IF object_id('dbo.item_unit_cost_per_po_per_receive_dt_avg2') IS not  NULL 
                        drop table dbo.item_unit_cost_per_po_per_receive_dt_avg2

		                select a.InvoiceDate receive_dt, OrderNo ordernr, ItemNo artcode,  isnull(qtyIn, 0) - isnull(qtyOut,0) Qty
		                , (isnull(qtyIn, 0) - isnull(qtyOut,0)) * PriceAUD amt , PriceAUD unit_cost 
		                into item_unit_cost_per_po_per_receive_dt_avg2
		                 from @TranHistorybackupTB
		                  a  left outer join apvenfil_sql v on (a.Vend_No = v.vend_no)
		                where [type] = 'Purchase'  and orderno is not null 
		                and a.loc in 
		                ('P','R','T','V','PM','PC', 'BA', 'NF', 'SY','BC','VK' ,'VB')  and  (commodity_cd_5 is null or commodity_cd_5 not in ('OFF','ZZZ')) 
                        and a.vend_no not in(select '          '+vend_no from APVENFIL_SQL where vend_name like '%livingstone%')
                    ";
            create_item_unit_cost_per_po_per_receive_dt_avg2_sql = ProcessParamInSql(create_item_unit_cost_per_po_per_receive_dt_avg2_sql, param1);

            dBHandler.ExecuteNonQuery(create_item_unit_cost_per_po_per_receive_dt_avg2_sql, SqlServerDB.Servers.netcrmau, timeout: 3000);

            logsqlexecution("step 1 complete");
            //Console.WriteLine("step 1 complete");
            // 2   create new        item_unit_cost_per_receive_dt_avg2   can be run multiple time 

            String create_item_unit_cost_per_receive_dt_avg2_sql =
                  @"
		 
                          IF object_id('dbo.item_unit_cost_per_receive_dt_avg2 ') IS not  NULL 
                          drop table dbo.item_unit_cost_per_receive_dt_avg2 
                        select * 
                        into item_unit_cost_per_receive_dt_avg2 
                        from ( 
		                         select receive_dt, artcode , sum(qty) qty , sum(amt)  amt, max( ordernr ) lastPONo , max( isnull( b.type ,'' )  ) KitType
		                        
		                         from item_unit_cost_per_po_per_receive_dt_avg2 a 
		                         left outer join vKITBOMItem b 
		                         on (a.artcode = b.item_no)
		                         group by receive_dt , artcode  
                        ) a  
                  ";
            dBHandler.ExecuteNonQuery(create_item_unit_cost_per_receive_dt_avg2_sql, SqlServerDB.Servers.netcrmau, timeout: 3000);
            logsqlexecution("step 2 complete");
            //Console.WriteLine("step 2 complete");

            // 3 insert into item_unit_cost_per_receive_dt_avg2 (created in last step) with new record


            String insert_item_unit_cost_per_receive_dt_avg2_sql =
                  @"

                            insert into item_unit_cost_per_receive_dt_avg2 (receive_dt, artcode, qty, amt, lastPONo, KitType )

                            select '19000101', a.item_no, 1,  isnull(b.usernumber_03, c.price) price, '  0', isnull( e.type ,'' ) KitType  
                            from ( 
                                 select distinct item_no from inv_summary2
                                 ) a   
                            left outer join items b 
                                        on (a.item_no = b.itemcode and b.usernumber_03 <> 0 )
                            left outer join  (  select item_no , max(price) price from iminvloc_sql group by item_no ) c 
                                       on (a.item_no = c.item_no  and c.price <> 0 ) 
                            left outer join (
                                            select distinct artcode from item_unit_cost_per_receive_dt_avg2  where qty > 0  and amt > 0 
                                           ) d 
	                                   on (a.item_no = d.artcode )
                            left outer join vKITBOMItem e 
                                       on (a.item_no = e.item_no)
                            where d.artcode is null  and isnull(  isnull(b.usernumber_03, c.price) ,0 ) > 0 


                            insert into item_unit_cost_per_receive_dt_avg2 (receive_dt, artcode, qty, amt, lastPONo, KitType  )

                            select '19000101', a.item_no, 1, isnull(b.usernumber_03, c.price) price, '  0', isnull( e.type ,'' ) KitType  
                            from ( 
		                            select  distinct b.comp_item_no item_no
		                            from item_unit_cost_per_receive_dt_avg2 a 
		                            left outer join bmprdstr_sql b 
		                                  on (a.artcode = b.item_no )
		                            left outer join item_unit_cost_per_receive_dt_avg2 c 
		                                  on (b.comp_item_no  = c.artcode)
		                            where a.kittype <> ''   and c.receive_dt is  null 
                             ) a left outer join items b 
                             on (a.item_no = b.itemcode and b.usernumber_03 <> 0 )
                            left outer join  
                            (  select item_no , max(price) price from iminvloc_sql group by item_no ) c 
                            on (a.item_no = c.item_no  and c.price <> 0 ) 
                             left outer join vKITBOMItem e on (a.item_no = e.item_no)


                  ";
            dBHandler.ExecuteNonQuery(insert_item_unit_cost_per_receive_dt_avg2_sql, SqlServerDB.Servers.netcrmau, timeout: 3000);
            logsqlexecution("step 3 complete");
            //Console.WriteLine("step 3 complete");


            // 4 recreate kit_receive_cost_avg2

            String recreate_kit_receive_cost_avg2_sql =
                 @"
                        IF object_id('dbo.kit_receive_cost_avg2') IS not  NULL 
                        drop table dbo.kit_receive_cost_avg2

                        select  a.artcode, a.KitType,c.receive_dt  
                        into dbo.kit_receive_cost_avg2
                          from item_unit_cost_per_receive_dt_avg2 a 
                          left outer join bmprdstr_sql b 
                          on (a.artcode = b.item_no )
                        left outer join item_unit_cost_per_receive_dt_avg2 c 
                        on (b.comp_item_no  = c.artcode)
                        where a.kittype <> '' and a.lastPONo  = '  0'
                        group by a.artcode, a.KitType,c.receive_dt

                  ";
            dBHandler.ExecuteNonQuery(recreate_kit_receive_cost_avg2_sql, SqlServerDB.Servers.netcrmau, timeout: 3000);
            logsqlexecution("step 4 complete");
            //Console.WriteLine("step 4 complete");


            // 5  insert into item_unit_cost_per_receive_dt_avg2 with new record 

            String insert_item_unit_cost_per_receive_dt_avg2_sql2 =
                @"
                          insert into item_unit_cost_per_receive_dt_avg2 (receive_dt, artcode, qty, amt, lastPONo, KitType )  
                         select  receive_dt, artcode 
                        ,  case when KitType = 'KIT' then   sum( qty_per_par  * qty) 
                                      else sum( qty_per_par  * qty  )   end qty
                          , case when KitType = 'KIT' then   sum( qty_per_par  * amt) 
                                      else sum( qty_per_par  * amt  * 1.3)   end unit_cost, '  0',   KitType 
                        from ( 
                         select a.artcode, a.KitType, a.receive_dt,  b.qty_per_par,  c.qty , c.amt , c.receive_dt comp_receive_dt
                        , row_number() over(partition by a.artcode, a.receive_dt  order by c.receive_dt desc) rank  
                          from kit_receive_cost_avg2 a 
                         left outer join bmprdstr_sql b 
                         on (a.artcode  = b.item_no)
                        left outer join item_unit_cost_per_receive_dt_avg2 c 
                        on ( b.comp_item_no = c.artcode and a.receive_dt >= c.receive_dt )
                        ) a where a.rank = 1 
                         group by artcode, KitType, receive_dt 
                  ";
            dBHandler.ExecuteNonQuery(insert_item_unit_cost_per_receive_dt_avg2_sql2, SqlServerDB.Servers.netcrmau, timeout: 3000);
            logsqlexecution("step 5 complete");
            //Console.WriteLine("step 5 complete");

            // 6  recreate StockPerReportingDate2   

            String recreate_StockPerReportingDate2 =
                @"
                                IF object_id('dbo.StockPerReportingDate2') IS not  NULL 
                                drop table dbo.StockPerReportingDate2
                                
                                select *, sum(Qty) over(Partition by ItemNo ORDER BY InvoiceDate ROWS UNBOUNDED PRECEDING ) RunningTotal 
                                into StockPerReportingDate2 
                                from ( 
                                     select ItemNo, InvoiceDate, sum( isnull(qtyIn, 0) - isnull(qtyOut,0) ) Qty			 
	                                from " + bk.TranHistorybackupTB + @" 
	                                where  loc in ('P','R','T','V','PM','PC', 'BA', 'NF', 'SY','BC','VK' ,'VB')
	                                group by ItemNo, InvoiceDate
                                ) a 

                                alter table StockPerReportingDate2 add StockDate varchar(10) 

                                update StockPerReportingDate2 
			                     set StockDate = convert(varchar, InvoiceDate, 112 ) 

                  ";
            dBHandler.ExecuteNonQuery(recreate_StockPerReportingDate2, SqlServerDB.Servers.netcrmau, timeout: 3000);

            logsqlexecution("step 6 complete");
            //Console.WriteLine("step 6 complete");


            // 7 recreate StockReceivingCost2  
            String recreate_StockReceivingCost2 =
                @"
                         IF object_id('dbo.StockReceivingCost2') IS not  NULL 
                         drop table dbo.StockReceivingCost2
                         
                          select * 
                          into StockReceivingCost2 
                          from ( 
			                        select a.*, b.RunningTotal, row_number() over(partition by a.artcode, a.receive_dt order by b.StockDate desc) rank 
			                        from item_unit_cost_per_receive_dt_avg2 a 
			                        left outer join StockPerReportingDate2 b 
			                        on ( a.artcode = b.ItemNo and a.receive_dt >= b.StockDate)
                          ) a 
                          where rank = 1 
     
   
                          alter table StockReceivingCost2 add AvgCost decimal(18,3), PreviousAvgCost decimal(18,3) 
   
                          delete from StockReceivingCost2 where qty <=0 

                          delete from  StockReceivingCost2  where artcode in (  'FRTSUP', 'FRTIMPORT','FRTIMPORT-G','FRTLOCAL' , 'PRINTING' ) 
   
                          
                          update StockReceivingCost2 set previousavgcost = null , avgcost = null 
                          
                      
                          update StockReceivingCost2 
                          set AvgCost = amt * 1.0 / qty 
                          where isnull(RunningTotal, 0) - qty <=0 and qty > 0 and amt > 0 

                          alter table StockReceivingCost2 
                          add UnitCost  decimal(18,3)

                          update StockReceivingCost2 set UnitCost = amt * 1.0 / qty 
                  ";
            dBHandler.ExecuteNonQuery(recreate_StockReceivingCost2, SqlServerDB.Servers.netcrmau, timeout: 3000);


            logsqlexecution("step 7 complete");
            //Console.WriteLine("step 7 complete");

            // 8 assign the remaining avgcost , do it until the messages

            String assign_remaining_avgcost_sql =
                @"
                              
                                  declare @idx int
                                  set @idx = 1 
                                  while @idx <= 500  and exists(
		                                 ---- select availbe items to assign for the next avgcost
                                       select * from 
	                                  ( 
		                                  select * , last_value(AvgCost) over(partition by artcode  order by receive_dt ROWS  between unbounded PRECEDING and 1 PRECEDING )  LastAvgCost 
		                                  from StockReceivingCost2 
		                                  --order by artcode, receive_dt 
	                                  ) a where a.avgcost is null and lastavgcost is not null 

                                  )
                                  begin 
		                                  begin tran
				                                  update b set b.previousavgcost = a.LastAvgCost 
				                                  from ( 
						                                  select * 
						                                  from 
						                                  ( 
								                                  select * , last_value(AvgCost) over(partition by artcode  order by receive_dt ROWS  between unbounded PRECEDING and 1 PRECEDING )  LastAvgCost 
								                                  from StockReceivingCost2 
								                                 --order by artcode, receive_dt 
						                                  ) a
						                                  where a.avgcost is null and lastavgcost is not null 
					                                  ) a 
				                                  left outer join StockReceivingCost2 b 
				                                  on (a.artcode = b.artcode and a.receive_dt= b.receive_dt )

				                                  /*
				                                  select *, a.amt * 1.0 / a.qty UnitCost
				                                  ,  case when runningtotal - qty > 0 then   (  ( runningtotal - qty) * previousavgcost + amt  ) * 1.0 / a.runningtotal else a.amt * 1.0 / a.qty end NewAvgCost
					                                from StockReceivingCost a
				                                  where a.avgcost is null and previousavgcost is not null 
				                                  */

				                                  update a set a.avgcost = case when runningtotal - qty > 0 then   (  ( runningtotal - qty) * previousavgcost + amt  ) * 1.0 / a.runningtotal else a.amt * 1.0 / a.qty end 
				                                  from StockReceivingCost2 a
				                                  where a.avgcost is null and previousavgcost is not null 
   
				                                  set @idx = @idx + 1 
				                                  print '==> ' + cast (@idx as varchar)
		                                  commit tran
                                  end
   

                  ";
            dBHandler.ExecuteNonQuery(assign_remaining_avgcost_sql, SqlServerDB.Servers.netcrmau, timeout: 3000);

            logsqlexecution("step 8 complete");
            //Console.WriteLine("step 8 complete");


            // 9 udpate StockReceivingCost2

            String update_StockReceivingCost2_sql = @"   
                                            update b set b.avgcost = a.newavgcost 
                                               from 
                                               ( 
                                                          select * from 
			                                             ( 
				                                              select *, row_number() over(partition by artcode order by receive_dt) rank2 , amt * 1.0 / qty NewAvgCost        
				                                               from StockReceivingCost2 
				                                               where avgcost is null and previousavgcost is null 
			                                             ) a 
		                                                where rank2 = 1 
                                               ) a 
                                               left outer join  StockReceivingCost2 b 
                                               on (a.artcode = b.artcode  and a.receive_dt = b.receive_dt)
                                        ";

            dBHandler.ExecuteNonQuery(update_StockReceivingCost2_sql, SqlServerDB.Servers.netcrmau, timeout: 3000);
            logsqlexecution("step 9 complete");
            //Console.WriteLine("step 9 complete");
            //  10 redo the avgcost
            dBHandler.ExecuteNonQuery(assign_remaining_avgcost_sql, SqlServerDB.Servers.netcrmau, timeout: 3000);

            logsqlexecution("step 10 complete");
            //Console.WriteLine("step 10 complete");


            // 11  sql update avg_wghted_cost2

            string update_avg_wghted_cost2_sql = @"   
                            update b set b.avg_wghted_cost2 = a.AvgCost  
                               from ( 
                                                  select * 
                                                  from ( 
                                                        select *, row_number() over(partition by a.item_no order by b.receive_dt desc ) rank2
                                                        from stock_value_v4_BKdb a 
							                            left outer join StockReceivingCost2 b 
							                            on (a.item_no = b.artcode and b.receive_dt <= '@lastDayLastMonth')
                                                     ) a where a.rank2 = 1 
                                     ) a 
                               left outer join stock_value_v4_BKdb b 
                               on (a.item_no = b.item_no )
                            ";
            update_avg_wghted_cost2_sql = update_avg_wghted_cost2_sql.Replace("@lastDayLastMonth", lastDayLastMonth.ToString("yyyyMMdd"));
            dBHandler.ExecuteNonQuery(update_avg_wghted_cost2_sql, SqlServerDB.Servers.netcrmau, timeout: 3000);
            logsqlexecution("step 11 complete");
            //Console.WriteLine("step 11 complete");

            string update_avg_wghted_cost2_With_repack = @" exec Adjust_Stock_value_v4_BKdb_ON_avg_wghted_cost2_BY_repack  ";
            
            dBHandler.ExecuteNonQuery(update_avg_wghted_cost2_With_repack, SqlServerDB.Servers.netcrmau, timeout: 3000);
            logsqlexecution("step 12 complete");





            //finally back up 
            BackupTable bk_end = new BackupTable(lastDayLastMonth, lastDayLastLastMonth, "end");

            logsqlexecution("sql run finished.");

            //Console.ReadKey();

        }


        public static String ProcessParamInSql(String sql, Dictionary<String, String> param)
        {
            if (param.Count > 0)
            {
                foreach (KeyValuePair<string, string> item in param)
                {
                    sql = sql.Replace(item.Key, item.Value);
                }
            }
            return sql;
        }
        public static void logsqlexecution(string notes)
        {
            string sql = @"
                            insert into StockValuationReportLog(logtime, notes)
                            values (GETDATE(), '@notes')
                            ";

            sql = sql.Replace("@notes", notes);

            dBHandler.ExecuteNonQuery(sql, SqlServerDB.Servers.netcrmau, timeout: 3000);
        }

    }

}
