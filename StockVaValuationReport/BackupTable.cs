﻿using Livingstone.DBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockVaValuationReport
{
    class BackupTable
    {
        private static IDBHandler dBHandler = new SqlServerDB();
        public DateTime lastDayLastMonth;
        public DateTime lastDayLastLastMonth;

        public string TranHistorybackupTB = "";

        public BackupTable(DateTime lastDayLastMonth, DateTime lastDayLastLastMonth, string start_End)
        {
            if(start_End == "start")
            {
                backupTableProcess(lastDayLastMonth, lastDayLastLastMonth);
            }
            else if(start_End == "end")
            {
                backupTableProcess_end(lastDayLastMonth, lastDayLastLastMonth);
            }
            
        }
        
        private void backupTableProcess(DateTime lastDayLastMonth, DateTime lastDayLastLastMonth)
        {

            ////back up Liv_vu_ItemTransactionHistory
            this.TranHistorybackupTB = "[backup].dbo.Liv_vu_ItemTransactionHistory_" + lastDayLastMonth.ToString("yyyyMMdd");
            String TranHistorybackup_source = "dbo.Liv_vu_ItemTransactionHistory";
            CreateBackUpTableNetcrmau(TranHistorybackupTB, TranHistorybackup_source);

            
            // back up imitmidx_sql
            String imitmidx_sqlbackupTB = "[backup].dbo.imitmidx_sql_stock_value_" + lastDayLastMonth.ToString("yyyyMMdd");
            String imitmidx_sql_source = "dbo.imitmidx_sql";
            CreateBackUpTableNetcrmau(imitmidx_sqlbackupTB, imitmidx_sql_source);


            // back up iminvloc_sql
            String iminvloc_sql_backupTB = "[backup].dbo.iminvloc_sql_stock_value_" + lastDayLastMonth.ToString("yyyyMMdd");
            String iminvloc_sql_Source = "dbo.iminvloc_sql";
            CreateBackUpTableNetcrmau(iminvloc_sql_backupTB, iminvloc_sql_Source);


        }

        private void backupTableProcess_end(DateTime lastDayLastMonth, DateTime lastDayLastLastMonth)
        {
            //backup stock_value_v4_BKdb
            String stock_value_v4_BKdb_source = "stock_value_v4_BKdb";
            String stock_value_v4_BKdb_TB = "[backup].dbo.stock_value_v4_BKdb_" + lastDayLastMonth.AddDays(1).ToString("yyyyMMdd");
            CreateBackUpTableNetcrmau(stock_value_v4_BKdb_TB, stock_value_v4_BKdb_source);


            //backup     item_unit_cost_per_po_per_receive_dt_avg2
            String item_unit_cost_per_po_per_receive_dt_avg2_TB = "[backup].dbo.item_unit_cost_per_po_per_receive_dt_avg2_ye" + lastDayLastMonth.ToString("yyyyMMdd");
            String item_unit_cost_per_po_per_receive_dt_avg_Source = "dbo.item_unit_cost_per_po_per_receive_dt_avg2";
            CreateBackUpTableNetcrmau(item_unit_cost_per_po_per_receive_dt_avg2_TB, item_unit_cost_per_po_per_receive_dt_avg_Source);

            //backup     item_unit_cost_per_receive_dt_avg2
            String item_unit_cost_per_receive_dt_avg2_TB = "[backup].dbo.item_unit_cost_per_receive_dt_avg2_ye" + lastDayLastMonth.ToString("yyyyMMdd");
            String item_unit_cost_per_receive_dt_avg2_Source = "item_unit_cost_per_receive_dt_avg2";
            CreateBackUpTableNetcrmau(item_unit_cost_per_receive_dt_avg2_TB, item_unit_cost_per_receive_dt_avg2_Source);

            //backup     StockPerReportingDate2
            String StockPerReportingDate2_TB = "[backup].dbo.StockPerReportingDate2_ye" + lastDayLastMonth.ToString("yyyyMMdd");
            String StockPerReportingDate2_Source = "StockPerReportingDate2";
            CreateBackUpTableNetcrmau(StockPerReportingDate2_TB, StockPerReportingDate2_Source);

            //backup     StockReceivingCost2
            String StockReceivingCost2_TB = "[backup].dbo.StockReceivingCost2_ye" + lastDayLastMonth.ToString("yyyyMMdd");
            String StockReceivingCost2_Source = "StockReceivingCost2";
            CreateBackUpTableNetcrmau(StockReceivingCost2_TB, StockReceivingCost2_Source);

        }
        public void CreateBackUpTableNetcrmau(String newtableName, String tableSource)
        {
            String sqlcommand = @"
                                  select * 
                                  into @newtable
                                  from  @Source
                                ";
            if (checkTableExist(newtableName) is null)
            {

                sqlcommand = sqlcommand.Replace("@newtable", newtableName);
                sqlcommand = sqlcommand.Replace("@Source", tableSource);

                dBHandler.ExecuteNonQuery(sqlcommand, SqlServerDB.Servers.netcrmau);
                Console.WriteLine("create table :" + newtableName + "   for : " + tableSource);
            }
            else
            {
                Console.WriteLine(newtableName + " already exist.......");
            }

        }

        private static long? checkTableExist(String tablename)
        {

            String checkSql = @"select object_id('@tablename')";

            checkSql = checkSql.Replace("@tablename", tablename);

            return dBHandler.getInt64(checkSql, SqlServerDB.Servers.netcrmau);

        }


    }
}
