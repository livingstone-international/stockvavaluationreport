﻿using Livingstone.DBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockVaValuationReport
{
    public static class QuantityAndSales
    {
        private static IDBHandler dBHandler = new SqlServerDB();

        public static void CreateQuantityAndSalesTables()
        {
            string QuantityAndSalesSql = @"
                          
                        --- --- --- --- --- --- --- --- item sales quantity  and total sales ------------------------------------------

                        if object_id('[backup].[dbo].itemsalesTemp') is not null
                        drop table [backup].[dbo].itemsalesTemp


                            select lin.artcode as productCode, sum( isnull( lin.esr_aantal, 0)) as totalQtySold , sum(isnull(lin.koers * lin.prijs_n * lin.esr_aantal, 0)) as totalSales
                            into [backup].[dbo].itemsalesTemp
	                        from frhsrg lin
	                        left join frhkrg hdr
		                        on (lin.faknr = hdr.faknr)
                             left join ARCUSFIL_SQL cus
	                        on (hdr.debnr = cus.debnr)
	                        left join IMITMIDX_SQL itm
                               on (lin.artcode = itm.item_no)
                        left join IMCATFIL_SQL cat
                               on (itm.item_filler = cat.prod_cat)
	                        where 
	                             lin.fakdat >= DATEADD(YEAR, -1,GETDATE()) and lin.fakdat <= GETDATE()
		                        and hdr.fak_soort = /*Sales Order*/'V'
		                        and lin.artcode NOT LIKE '%FRT%'
		                        and lin.prijs_n >0
                               and cat.is_physical_item = 1 
		                       
	                        group by lin.artcode


	                    alter table [backup].[dbo].itemsalesTemp
	                    add last_sold_dt int 	

	                    if  object_id('tempdb..#templastinvoice') is not null
	                    drop table #templastinvoice

	                    Select *
	                    into #templastinvoice
	                    from 
	                    (
	                           --item sales history order by bill date
			                    select  ROW_NUMBER() over ( partition by a.item_no order by a.billed_dt desc) as ng, a.item_no,a.billed_dt
			                    from ordhstbyitem a with (nolock)  --only sales history, no purchase and count
			                    
			                    where a.unit_price > 0 
			                           -- and   a.item_no = 'MEA30B'   --check specific item
					                    --order by billed_dt desc
	                    ) c
	                    where c.ng = 1


	                    update a
	                    set a.last_sold_dt = b.billed_dt
	                    --select a.*
	                    from [backup].[dbo].itemsalesTemp a
	                    left join #templastinvoice b
	                    on(a.productCode = b.item_no)
                        



                        ------------------------------total qty on hand query in product details form----------------------------------------

                        if object_id('[backup].[dbo].TotalqtyTableTemp') is not null
                        drop table [backup].[dbo].TotalqtyTableTemp

                                    SELECT item_no,
                                        SUM(on_hand) [On Hand]
                                        ,SUM([Assembling]) [Assembling] 
                                        ,SUM(allocatted) [Allocated]
                                        ,SUM(on_hand+on_po-allocatted) [Available]
                                        ,SUM(on_po) [On PO]
			                        into [backup].[dbo].TotalqtyTableTemp
                                    FROM (
                                        SELECT sd.item_no,
		                                    CAST(sd.[On Hand] AS bigint) on_hand
		                                    ,CAST(ISNULL(MIN(FLOOR(tb3.[On Hand] / tb2.qty_per_par)),0) AS bigint) [Assembling]
		                                    ,CAST(sd.Allocated AS bigint) allocatted
		                                    ,CAST(sd.[On PO] AS bigint) on_po
                                        FROM vitem_stock_details sd
	                                    LEFT JOIN bmprdstr_sql tb2
	                                    ON sd.item_no = tb2.item_no
		                                    AND sd.warehouse = tb2.loc
	                                    LEFT JOIN vitem_stock_details tb3
	                                    ON tb2.comp_item_no = tb3.item_no
		                                    AND tb3.warehouse = sd.warehouse
	                                    WHERE sd.item_no in (select item_no from [backup].[dbo].MonthReport)
                            
                                           AND sd.warehouse IN ( select loc_code from warehouse where country = 'Australia' and Status = 'Active' and stock = 1 )
                                
                                       GROUP BY sd.item_no,sd.[On Hand], sd.Allocated, sd.[On PO]

                                  ) Tb1
		                          group by item_no";


            dBHandler.ExecuteNonQuery(QuantityAndSalesSql, SqlServerDB.Servers.netcrmau);



        }



    }
}
