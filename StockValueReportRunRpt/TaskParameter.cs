﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockValueReportRunRpt
{
    public class TaskParameter
    {
        private string parameterName;

        public string ParameterName
        {
            get { return parameterName; }
            set { parameterName = value; }
        }

        private string parameterValue;

        public string ParameterValue
        {
            get { return parameterValue; }
            set { parameterValue = value; }
        }

        public TaskParameter()
        {
        }

        public TaskParameter(string parameterName, string parameterValue)
        {
            this.parameterName = parameterName;
            this.parameterValue = parameterValue;
        }



    }
}
