﻿using Livingstone.DBLib;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockValueReportRunRpt
{
    public class Tool
    {


        private static readonly IDBHandler dBHandler = new SqlServerDB();
        public static String folder = "";
        public static string ReadSetting(string key)
        {
            try
            {
                var appSettings = ConfigurationManager.AppSettings;
                string result = appSettings[key] ?? "Not Found";
                return result;
            }
            catch (ConfigurationErrorsException)
            {
                return string.Empty;
            }
        }
        /// <summary>Get excel file name for export file </summary>
        /// <param name="location">HK or AU</param>
        /// <param name="fileName">excel file name you want to speficy</param>
        /// <param name="path">excel file path you want to save</param>
        /// <returns>excel file name</returns>
        public static string GetExcelFileName(string path, string location, string fileName)
        {

            if (folder == "")
            {
                folder = path + DateTime.Now.Year.ToString() + DateTime.Now.ToString("MM") + DateTime.Now.ToString("dd") + "_" + DateTime.Now.Hour.ToString() + "_" + DateTime.Now.Minute;
            }

            //folder will not change for the program, it's date and time,so it is static field in the Tool class. while report folder is the current report's folder, check everytime. 
            string reportFolder = "";



            if (Directory.Exists(reportFolder) == false)
                Directory.CreateDirectory(reportFolder);
            //set export file path
            fileName = reportFolder + "\\" + fileName;



            if (location.ToUpper() == "HK")
                return fileName + " hk.xls";
            else
                return fileName + ".xls";
        }




















    }
}
