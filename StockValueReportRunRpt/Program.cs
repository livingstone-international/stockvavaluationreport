﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace StockValueReportRunRpt
{
    class Program
    {
        //auto run rpt to generate reports
        static void Main(string[] args)
        {

            string s = DateTime.Now.ToString("yyyyMMdd");

            try
            {

                TaskMgr taskMgr = new TaskMgr();

                for (int i = 0; i < taskMgr.ReportTasks.Count; i++)
                {
                    TaskInfo task = taskMgr.ReportTasks[i];

                    ReportDocument cryRpt = new ReportDocument();
                    //Load the report from the spefic path
                    cryRpt.Load(task.CrystalReportFilePath);
                    cryRpt.Refresh();

                    //if patr ameter is not null then add the patrameter name and value to the crystal report
                    if (task.Parameters != null)
                    {
                        foreach (TaskParameter patra in task.Parameters)
                        {
                            cryRpt.SetParameterValue(patra.ParameterName, patra.ParameterValue);
                        }
                    }

                    //export the crytal report as excel file 
                    string ExportReportPathLocation = Tool.ReadSetting("ExportReportPathLocation");
                    cryRpt.ExportToDisk(ExportFormatType.Excel, Tool.GetExcelFileName(ExportReportPathLocation, task.ReportLoc, task.ExportFileName));
                    cryRpt.Dispose();


                }




            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());

            }





        }
    }
}
