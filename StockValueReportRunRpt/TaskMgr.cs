﻿using Livingstone.DBLib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockValueReportRunRpt
{
    public class TaskMgr
    {
        private static readonly IDBHandler dBHandler = new SqlServerDB();
        private List<TaskInfo> reportTasks;

        public List<TaskInfo> ReportTasks
        {
            get { return reportTasks; }
            set { reportTasks = value; }
        }
        public TaskMgr()
        {
            reportTasks = new List<TaskInfo>();
            LoadReports();
        }

        private void LoadReports()
        {

            List<TaskParameter> parameters = new List<TaskParameter>();
            parameters.Add(new TaskParameter("Years", "8"));
            parameters.Add(new TaskParameter("Warehouse", "Stock WH"));
            string reportName = "stock value by grp_detailed_pprice_v4_bkdb" + DateTime.Now.ToString("yyyyMMdd");
            reportTasks.Add(new TaskInfo("Stock Value by Grp_detailed_pprice_v4_BKdb", parameters, "AU", reportName, GetFilePath("AU", "Stock Value by Grp_detailed_pprice_v4_BKdb")));



        }



        private string GetFilePath(string location, string fileName)
        {
            //add some logic for copying ? 
            string Filepath = Directory.GetCurrentDirectory() + @"\rpt\" + fileName;
            return Filepath;
        }


    }
}
