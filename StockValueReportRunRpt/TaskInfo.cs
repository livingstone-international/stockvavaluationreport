﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockValueReportRunRpt
{
    public class TaskInfo
    {
        private string crystalReportfileName;

        public string CrystalReportfileName
        {
            get { return crystalReportfileName; }
            set { crystalReportfileName = value; }
        }

        private string exportFileName;

        public string ExportFileName
        {
            get { return exportFileName; }
            set { exportFileName = value; }
        }


        private List<TaskParameter> parameters = new List<TaskParameter>();

        public List<TaskParameter> Parameters
        {
            get { return parameters; }
            set { parameters = value; }
        }

        private string reportLoc;

        public string ReportLoc
        {
            get { return reportLoc; }
            set { reportLoc = value; }
        }

        private string crystalReportFilePath;

        public string CrystalReportFilePath
        {
            get { return crystalReportFilePath; }
            set { crystalReportFilePath = value; }
        }



        public TaskInfo()
        {
        }

        public TaskInfo(string CrystalReportfileName, List<TaskParameter> parameters, string reportLoc, string exportFileName, string crystalReportFilePath)
        {
            this.CrystalReportfileName = CrystalReportfileName;
            this.parameters = parameters;
            this.reportLoc = reportLoc;
            this.exportFileName = exportFileName;
            this.crystalReportFilePath = crystalReportFilePath;
        }












    }
}
